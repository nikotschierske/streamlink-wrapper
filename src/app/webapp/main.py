from flask import Flask

app = Flask(__name__)

@app.route("/rest/getRecordings", methods=["GET"])
def hello_world():
    return ""


def main():
    app.run() #Run the flask app; this is not asynchronous

if __name__ == "__main__":
    main()
