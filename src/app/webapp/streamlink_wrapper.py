import streamlink
import ffmpeg_streaming
from ffmpeg_streaming import Formats
from pathlib import Path
import os

recordingStreams = []

def getStreams(url: str):
    return streamlink.streams(url)

def getBestStream(url: str):
    return streamlink.streams(url)["best"]

def recordStream(url: str, relativePath: str):
    """records a hls stream to an mkv"""
    streamURL = getBestStream(url).url
    filename = url.split("/")[-1] + ".mkv"
    
    video = ffmpeg_streaming.input(streamURL) 
    stream = video.stream2file(Formats.hevc())
    
    if not os.path.exists(relativePath):
        print(f"file path given for stream '{url}' doesn't exist \n creating necessary folders") #TODO replace with logger | warning
        path = Path(relativePath)
        path.mkdir(parents=True)
    
    filepath = os.path.join(relativePath, filename)
    stream.output(filepath)
    
def debug():
    recordStream("https://www.twitch.tv/papaplatte", ".")
    

if __name__ == "__main__":
    debug()